#ifndef DISPLAY_CONSOLE_H_INCLUDED
#define DISPLAY_CONSOLE_H_INCLUDED
#include <iostream>
#include "console.h"
#include "Personnage.h"
#include "Case.h"
#include <vector>

void display_console(std::vector<std::vector<Case>> vec, int* px, int* py);
std::vector< std::vector<Case> > init_vector(Case a, Case b);


#endif // DISPLAY_CONSOLE_H_INCLUDED
