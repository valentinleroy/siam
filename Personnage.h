#ifndef PERSONNAGE_H_INCLUDED
#define PERSONNAGE_H_INCLUDED
#include <iostream>

class Personnage
{
protected:
    int m_direction;
    unsigned int m_resistance;
    //bool m_selectionable;
    int m_type;
public:
    //Personnage(int _x, int _y,int _direction,bool _surPlateau, unsigned int _resistance, bool _selectionable, int _type);
    Personnage(int _direction, int _resistance, int _type);
    Personnage();
    ~Personnage();
    int getdirection();
    //bool getOnPlateau();
    unsigned int getResistance();
    int get_type();
    void setdirection(int d);
};

class Elephant : public Personnage
{
public:
    Elephant();
    Elephant(int direction);
    ~Elephant();
};

class Rhinoceros : public Personnage
{
public:
    Rhinoceros();
    Rhinoceros(int direction);
    ~Rhinoceros();
};

class Montagne : public Personnage
{
public:
    Montagne();
    ~Montagne();
};



#endif // PERSONNAGE_H_INCLUDED
