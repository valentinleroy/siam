#include <iostream>
#include "console.h"
#include "Jeu.h"

using namespace std;

int menu()
{
    bool quit = false;
    int choix; // choix utiliser dans le switch du menu
    int x(7);
    Console* pConsole = NULL;
    system("cls");
    // Alloue la m�moire du pointeur
    pConsole = Console::getInstance();
    x=7;
    //D�but du menu
    pConsole->setColor(COLOR_RED);
    pConsole->gotoLigCol(3, 20);
    cout << "***  Bienvenue dans notre Jeu de Siam  ***" << endl << endl;
    cout << "Cree par Valentin Leroy, Maxime Fontaine et Ghislain Kicinski" << endl << endl;
    cout << "1 : Demarrer une partie" << endl;
    cout << "2 : Lire les regles" << endl;
    cout << "3 : Quitter" << endl << endl;

    pConsole->gotoLigCol(7,0);
    while (!quit)
    {
        // Si on a appuy� sur une touche du clavier
        if (pConsole->isKeyboardPressed())
        {
            // R�cup�re le code ASCII de la touche
            int key = pConsole->getInputKey();
            if(key==72)
                {
                    if(x>7)
                    x--;
                }
            else if(key==80)
                {
                        if(x<9)
                        x++;
                }
            if (key == 13) // 27 = touche escape
            {
                quit = true;
                pConsole->gotoLigCol(7,0);
            }
             pConsole->gotoLigCol(x,0);
        }
    }
    switch((x-6))
    {
        case 1:
            jeu();
            break;
        case 2:
            display_rules();
            break;
        case 3:
            break;
    }

    // Lib�re la m�moire du pointeur !
    Console::deleteInstance();

    pConsole->setColor(COLOR_DEFAULT);
    return 0;
}
