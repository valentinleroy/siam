#include "R�gles.h"
#include "menu.h"
#include "console.h"

void display_rules()
{
    system("cls");
    bool quit = false;
    int choix; // choix utiliser dans le switch du menu
    int x(3);
    Console* pConsole = NULL;

    // Alloue la m�moire du pointeur
    pConsole = Console::getInstance();

    //D�but du menu
    pConsole->setColor(COLOR_RED);
    std::cout<<"Bienvenue dans les regles du jeu \n";
    std::cout<<"Entrez la selection pour avoir des details concernant les regles.\n";
    std::cout<<"A chaque tour du jeu, le joueur ne peut effectuer que 5 actions.\n";
    std::cout<<"1. Faire entrer un de ses pions sur une case vide du tableau.\n";
    std::cout<<"2. Deplacer un de ses pions sur une case vide du tableau\n";
    std::cout<<"3. Changer l'orientation de son personnage.\n";
    std::cout<<"4. Sortir un de ses pions de tableau s'il est sur une case vide du tableau\n";
    std::cout<<"5. Se deplacer ou pousser une piece juxtaposee\n";
    std::cout<<"6. Quitter les regles du jeu\n";
    pConsole->gotoLigCol(3,0);
    while (!quit)
    {
        // Si on a appuy� sur une touche du clavier
        if (pConsole->isKeyboardPressed())
        {
            // R�cup�re le code ASCII de la touche
            int key = pConsole->getInputKey();
            if(key==72)
                {
                    if(x>3)
                    x--;
                }
            else if(key==80)
                {
                        if(x<8)
                        x++;
                }
            if ((key == 13)&&(x==8)) // 27 = touche escape
            {
                quit = true;
                pConsole->gotoLigCol(7,0);
            }
            if (key==13)
            {
                if((x-2)==6)
                {
                    quit=true;
                }
                else
                {
                    sous_menu(x-2);
                }
            }
             pConsole->gotoLigCol(x,0);
        }
    }
    menu();
}

void sous_menu (int x)
{
        Console* pConsole = NULL;
        pConsole = Console::getInstance();
        system("cls");
        switch(x)
        {
        case 1:
            std::cout<<"Le joueur peut faire entrer un de ses pions sur n'importe quelle case en bordure du plateau a condition qu'elle soit libre\n";
            std::cout<<"Il peut faire entrer un pion sur la case en bordure en effectuant une poussee \n (voir deplacement).\n";
            std::cout<<"\n Retour : Appuyer sur n'importe quelle touche";
            while(!pConsole->isKeyboardPressed())
            {}
            display_rules();
            break;
        case 2:
            std::cout<<"Il est possible deplacer un pion uniquement en ligne droite,\n mais il est aussi possible de changer son orientation.\n";
            std::cout<<"Le changement de direction du personnage est possible\n avant et apres le deplacement.\n";
            std::cout<<"\n Retour : Appuyer sur n'importe quelle touche";
            while(!pConsole->isKeyboardPressed())
            {}
            display_rules();
            break;
        case 3:
            std::cout<<"Il est possible de changer la direction du pion \n avant le deplacement et apres le deplacement.\n Cependant il n'est pas possible si pendant le deplacement\n le pion en pousse un autre de changer sa direction apres le mouvement\n";
            std::cout<<"\n Retour : Appuyer sur n'importe quelle touche";
            while(!pConsole->isKeyboardPressed())
            {}
            display_rules();
            break;
        case 4:
            std::cout<<"Il est possible de sortir du plateau un pion situe \n en bord de plateau, cela compte comme un tour de jeu.\n";
            std::cout<<"\n Retour : Appuyer sur n'importe quelle touche";
            while(!pConsole->isKeyboardPressed())
            {}
            display_rules();
            break;
        case 5:
            std::cout<<"Il est possible de faire sortir un personnage du plateau en le poussant\n";
            std::cout<<"Il n'est possible que de pousser vers l'avant, \n un personnage de cote n'a pas de force de poussee,\n deux personnages l'un en face de l'autre ne peuvent se pousser.\n";
            std::cout<<"\n Retour : Appuyer sur n'importe quelle touche";
            while(!pConsole->isKeyboardPressed())
            {}
            display_rules();
            break;
        default :
            menu();
        }

}
