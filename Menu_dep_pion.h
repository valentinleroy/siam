#ifndef MENU_DEP_PION_H_INCLUDED
#define MENU_DEP_PION_H_INCLUDED
#include "Plateau.h"
#include "Joueur.h"

void menu_dep_pion(Plateau* p, Case c, int *x, int *y, Joueur* j);

#endif // MENU_DEP_PION_H_INCLUDED
