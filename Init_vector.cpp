#include <iostream>
#include "console.h"
#include "Personnage.h"
#include "Case.h"
#include "Display_console.h"
#include <vector>

std::vector< std::vector<Case> > init_vector(Case a, Case b)
{
    std::vector< std::vector<Case> > vec(6, std::vector<Case>(5));
    for(int i=0;i<6;i++)
    {
        for (int j=0;j<5;j++)
        {
            if(((j==2)&&(i==2))||((j==2)&&(i==3))||((j==2)&&(i==4)))
            {
                vec[i][j]=b;
            }
            else
            {
                vec[i][j]=a;
            }
        }
    }
    return vec;
}
