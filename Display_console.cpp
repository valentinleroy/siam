#include "Display_console.h"

void display_console(std::vector<std::vector<Case>> vec, int* px, int* py)
{
    bool quit;
    Personnage* a;
    int x(1),y(1);
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    pConsole->setColor(COLOR_GREEN);
    pConsole->gotoLigCol(1,1);
    for(int i=0;i<6;i++)
    {
        if(i==0)
            std::cout<<"";
        else
        std::cout<<(char)(i+96);
        for(int j=0;j<5;j++)
        {
            if(i==0)
            {
                std::cout<<" "<<j+1<<" |";
            }
            else
                {a=(vec[i][j]).getpersonnage();
                if(a==NULL)
                {
                    std::cout<<"   |";
                }
                else
                {
                    if(a->get_type()==-1)
                        std::cout<<" M |";
                    else if(a->get_type()==1)
                        std::cout<<" E"<<a->getdirection()<<"|";
                    else if(a->get_type()==2)
                        std::cout<<" R"<<a->getdirection()<<"|";
                }
                }

        }
        std::cout<<"\n";
    }
     while (!quit)
    {
        // Si on a appuy� sur une touche du clavier
        if (pConsole->isKeyboardPressed())
        {
            // R�cup�re le code ASCII de la touche
            int key = pConsole->getInputKey();
            if(key==72)
                {
                    if (x>1)
                        x--;
                }
            else if(key==80)
                {
                    if (x<5)
                        x++;
                }
            else if (key==77)
                {
                    if(y<=4)
                        y++;
                }

            else if(key==75)
            {
                if(y>1)
                    y--;
            }
            pConsole->gotoLigCol(3,42);
            std::cout<<x<<";"<<y<<";"<<key;
            pConsole->gotoLigCol(x+1,4*y-2);
            if (key == 13) // 27 = touche escape
            {
                quit = true;
                *px=x;
                *py=y;
                pConsole->gotoLigCol(7,0);
            }
        }
    }
}
