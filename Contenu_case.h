#ifndef CONTENU_CASE_H_INCLUDED
#define CONTENU_CASE_H_INCLUDED
#include "Plateau.h"
#include "Joueur.h"

void contenu_case(Plateau* p, Joueur* j);

#endif // CONTENU_CASE_H_INCLUDED
