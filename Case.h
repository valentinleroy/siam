#ifndef CASE_H_INCLUDED
#define CASE_H_INCLUDED
#include "Personnage.h"

class Case
{
private :
    int m_type=0;
    Personnage* m_a=NULL;
public:
    int gettype()
    {
        if(m_a==NULL)
        {
            return 0;
        }
        else
        {
            return m_a->get_type();
        }
    }
    void settype(int _type)
    {
        m_type=_type;
    }
    void setPersonnage(Personnage* _a)
    {
        m_a=_a;
    }
    int getdirection()
    {
        if(m_a==NULL)
        {
            return 0;
        }
        else
            return m_a->getdirection();
    }
    int getresistance()
    {
        if(m_a==NULL)
        {
            return 0;
        }
        else
            return m_a->getResistance();
    }
    Personnage* getpersonnage()
    {
        return m_a;
    }
};

#endif // CASE_H_INCLUDED
