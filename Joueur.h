#ifndef JOUEUR_H_INCLUDED
#define JOUEUR_H_INCLUDED
#include <iostream>
#include <vector>
#include "console.h"
#include "Personnage.h"
#include "Case.h"
#include "Display_console.h"
#include "Jeu.h"

class Joueur
{
private:
    int m_type_pion;
    int m_stock_pion;
    std::vector<Personnage> m_tab_pion;
public:
    void settype_pion (int _type_pion)
    {
       m_type_pion= _type_pion;
    }
    int gettype_pion() {return m_type_pion;}
    void setstock_pion(int _stock_pion) {m_stock_pion=_stock_pion;}
    int getstock_pion() {return m_stock_pion;}
    std::vector<Personnage> get_tab_pion() {return m_tab_pion;}
    void pop_pion() {m_tab_pion.pop_back();}
    Joueur(int _type_pion, int _stock_pion);
    ~Joueur();
};

#endif // JOUEUR_H_INCLUDED
