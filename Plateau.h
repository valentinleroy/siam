#ifndef PLATEAU_H_INCLUDED
#define PLATEAU_H_INCLUDED
#include "Case.h"
#include "Joueur.h"
#include <vector>

class Plateau
{
private:
    std::vector<std::vector<Case>> m_plateau;
public:
    void display_plateau();
    void selectionner_case(int* px, int* py, Joueur* j);
    void init_plateau();
    Plateau();
    Case getcase(int x, int y);
    void poser_pion(Case pion, int x, int y);
    bool case_libre(int x, int y);
    void supprimer_case(Joueur* j,int x, int y);
    void deplacer_pion(int x1, int y1, int x2, int y2);
    void deplacer_force_pion(int x1, int y1, int x2, int y2);
    void placer_pion(Joueur* j,int x, int y);
    void poussee_pion(int x1, int y1, int x2, int y2);
    void pions_restants(Joueur* j1, Joueur* j2);
    void placer_force_pion(Joueur* j,int x, int y, char direction);
    void poser_force_pion(Case pion, int x, int y);
    int poussee_force_pion(int x1, int y1, int x2, int y2);
};

#endif // PLATEAU_H_INCLUDED
