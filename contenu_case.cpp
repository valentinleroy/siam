#include "Contenu_case.h"
#include "Plateau.h"
#include "console.h"
#include "Jeu.h"
#include "Menu_dep_pion.h"
#include <stdlib.h>

void contenu_case(Plateau* p, Joueur* j)
{
    //D�claration des variables
    int x(1),y(1);
    int type_pion_joueur=j->gettype_pion(); //on recupere le genre du pion du joueur
    int *px=&x;
    int *py=&y;
    Case selection;

    //traitement
    p->display_plateau();
    p->selectionner_case(px,py,j);
    selection = p->getcase(x,y);

    //Moteur du jeu
    if(((x==0)||(y==0)||(x==4)||(y==4))&&(p->case_libre(x,y))==true)  //Si la selection est sur les cotes et que la case est libre
    {
        p->placer_pion(j,x,y);
    }
    else if (((x==0)||(y==0)||(x==4)||(y==4))&&(p->case_libre(x,y))==false) // si la selection est sur les cotes et que la case est occupee
    {
        std::cout<<"voulez vous selectionner ce pion (1) ou en faire rentrer un nouveau (2) \n";
        int choix;
        do{
        std::cin>>choix;
        }while(choix!=1 && choix!=2);
        switch(choix)
        {
        case 1:
            if(type_pion_joueur==selection.gettype())
            {
                ///Deplacement Pion
                menu_dep_pion(p,selection,px,py,j);
            }
            else
            {
                system("cls");
                std::cout<<"Vous ne pouvez pas selectionner un pion adverse !\n";
                std::cout<<x<<";"<<y<<";"<<type_pion_joueur<<";"<<selection.gettype();
                //system("PAUSE");
                contenu_case(p,j);
            }
            break;
        case 2:
            char direction_choisie;
            std::cout<<"Dans quelle direction voulez vous faire rentrer votre pion?[h,g,b,d]\n";
            do{
            std::cin>>direction_choisie;
            }while(direction_choisie!='b' && direction_choisie!='h' && direction_choisie!='d' && direction_choisie!='g');
            switch(direction_choisie)
            {
            case 'b':
                if(x==4)
                    std::cout<<"Impossible de faire rentrer un pion avec cette direction\n";
                else
                    {
                        p->poussee_force_pion(x,y,x+1,y);
                        p->placer_force_pion(j,x,y,direction_choisie);
                    }
                break;
            case 'h':
                if(x==0)
                    std::cout<<"Impossible de faire rentrer un pion avec cette direction\n";
                else
                {
                    p->poussee_force_pion(x,y,x-1,y);
                    p->placer_force_pion(j,x,y,direction_choisie);
                }
                break;
            case 'd':
                if(y==4)
                    std::cout<<"Impossible de faire rentrer un pion avec cette direction\n";
                else
                {
                    p->poussee_force_pion(x,y,x,y+1);
                    p->placer_force_pion(j,x,y,direction_choisie);
                }
                break;
            case 'g':
                if(y==0)
                    std::cout<<"Impossible de faire rentrer un pion avec cette direction\n";
                else
                {
                    p->poussee_force_pion(x,y,x,y-1);
                    p->placer_force_pion(j,x,y,direction_choisie);
                }
                break;
            }
            break;
        }
    }
    else if (p->case_libre(x,y)==true&&!((x==0)||(y==0)||(x==4)||(y==4))) //si la case n'est pas sur les cotes et que la case est libre
    {
        contenu_case(p,j);
    }
    else if((p->case_libre(x,y)==false)&&!((x==0)||(y==0)||(x==4)||(y==4))) //si la case n'est pas sur les cot�s et que la case est occup�e
    {
        ///menu deplacement joueur
        if(type_pion_joueur!=selection.gettype())
        {
            std::cout<<"vous ne pouvez pas selectionner un pion adverse!\n";
            system("PAUSE");
            contenu_case(p,j);
        }
        else
        {   ///deplacement pion
            menu_dep_pion(p,selection,px,py,j);
        }

    }
    p->display_plateau();
}
