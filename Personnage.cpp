#include "Personnage.h"

Personnage::Personnage(int _direction, int _resistance, int _type)
:m_direction(_direction),m_resistance(_resistance), m_type(_type)
{}


Personnage::Personnage()
            : m_direction(1), m_resistance(10), m_type(-1)
            {

            }

Personnage::~Personnage()
{}


int Personnage::get_type()
{
    return m_type;
}




unsigned int Personnage::getResistance()
{
    return m_resistance;
}

int Personnage::getdirection()
{
    return m_direction;
}

void Personnage::setdirection(int d)
{
    m_direction=d;
}

Elephant::Elephant()
        :Personnage(1,10,1)
{
}

Elephant::Elephant(int direction)
:Personnage(direction,10,1)
{}

Rhinoceros::Rhinoceros()
:Personnage(1,10,2)
{}

Rhinoceros::Rhinoceros(int direction)
:Personnage(direction,10,2)
{}

Montagne::Montagne()
:Personnage(0,-9,-1)
{}
