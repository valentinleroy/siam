#include "Plateau.h"
#include "Menu_dep_pion.h"

#include "console.h"
#include <stdlib.h>

void menu_dep_pion(Plateau* p,Case c, int* px, int* py,Joueur* j)
{
    //Declaration des variables
    int x1,y1,ret;
    int x2,y2;
    x1=(*px);
    y1=(*py);
    Case init,fin;

    //Initialisation console
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    pConsole->gotoLigCol(1,25);
    pConsole->setColor(COLOR_GREEN);


    std::cout<<"Vous avec selectionne une case avec un pion\n";
    pConsole->gotoLigCol(2,25);
    std::cout<<" qui vous appartient, selectionnez une case avec les\n";
    pConsole->gotoLigCol(3,25);
    std::cout<<"fleches pour deplacer votre pion sur celle ci";

    //Selection de la case pour le deplacement
    p->selectionner_case(&x2,&y2,j);
    init=p->getcase(x1,y1);
    fin=p->getcase(x2,y2);
    std::cout<<x2<<";"<<y2<<fin.gettype();
    //Ajouter une verification pour la totalit� du vecteur soit vertical soit horizontal
    if(( (x2==(x1+1))&& (y2==y1) || (y2==(y1+1)) && (x1==x2) || (x2==(x1-1)) && (y1==y2) || (y2==(y1-1)) && (x1==x2) ) && ( (fin.getpersonnage()) == NULL ))         //deplacement orthogonal et si le type de la case est null
       {
               p->deplacer_pion(x1,y1,x2,y2);
       }
    else if(!( (x2==(x1+1))&& (y2==y1) || (y2==(y1+1)) && (x1==x2) || (x2==(x1-1)) && (y1==y2) || (y2==(y1-1)) && (x1==x2) )&&((fin.getpersonnage())==NULL))
    {
        menu_dep_pion(p,c,px,py,j);
    }
    else if (((x2==(x1+1))&& (y2==y1) || (y2==(y1+1)) && (x1==x2) || (x2==(x1-1)) && (y1==y2) || (y2==(y1-1)) && (x1==x2)) &&(fin.getpersonnage())!=NULL)
        {
            ///Menu pouss�e
            p->poussee_pion(x1,y1,x2,y2);
        }
    else if((x1==x2)&&(y1==y2))  //Si on appuie sur backspace, la case est supprim�e
    {
        p->deplacer_pion(x1,y1,x2,y2);
    }
    else if((x2==(-1))&&(y2==(-1)))
    {
        p->supprimer_case(j,x1,y1);
    }

    p->display_plateau();

}
