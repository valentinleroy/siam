#include "Plateau.h"
#include "Case.h"
#include "console.h"
#include "Joueur.h"
#include "menu.h"
#include <stdlib.h>

Plateau::Plateau()
{
    std::vector< std::vector<Case> > vec(6, std::vector<Case>(5));
    m_plateau=vec;
}

void Plateau::init_plateau()
{
    Case a,m1,m2,m3;
    a.setPersonnage(NULL);
    m1.setPersonnage((new Personnage));
    for(int i=0;i<6;i++)
    {
        for (int j=0;j<5;j++)
        {
            if(((j==2)&&(i==2))||((j==2)&&(i==3))||((j==2)&&(i==4)))
            {
                m_plateau[i][j]=m1;
            }
            else
            {
                m_plateau[i][j]=a;
            }
        }
    }
}

void Plateau::display_plateau()
{
    system("cls");
    Personnage* a;
    char dir;
    for(int i=0;i<6;i++)
    {
        if(i==0)
            std::cout<<" ";
        else
        std::cout<<(char)(i+64);
        for(int j=0;j<5;j++)
        {
            if(i==0)
            {
                std::cout<<" "<<j+1<<" |";
            }
            else
            {
                a=(m_plateau[i][j]).getpersonnage();
                if(a==NULL)
                {
                    std::cout<<"   |";
                }
                else
                {
                    if(a->get_type()==-1)
                        {
                            std::cout<<" M |";
                        }
                    else if(a->get_type()==1)
                        {
                            if(a->getdirection()==2)
                                dir='d';
                            else if (a->getdirection()==-2)
                                dir='g';
                            else if(a->getdirection()==1)
                                dir='h';
                            else if(a->getdirection()==(-1))
                                dir='b';
                            std::cout<<" E"<<dir<<"|";
                        }
                    else if(a->get_type()==2)
                        {
                            if(a->getdirection()==2)
                                dir='d';
                            else if (a->getdirection()==-2)
                                dir='g';
                            else if(a->getdirection()==1)
                                dir='h';
                            else if(a->getdirection()==(-1))
                                dir='b';
                            std::cout<<" R"<<dir<<"|";
                        }
                }
          }

        }
        std::cout<<std::endl;
    }
}

void Plateau::selectionner_case(int *px, int*py, Joueur* j)
{
    int a = j->gettype_pion();
    bool quit;
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    pConsole->gotoLigCol(1,2);
    int x(0),y(1);
    while (!quit)
    {
        // Si on a appuy� sur une touche du clavier
        if (pConsole->isKeyboardPressed())
        {
            // R�cup�re le code ASCII de la touche
            int key = pConsole->getInputKey();
            if(key==72)
                {
                    if (x>0)
                        x--;
                }
            else if(key==80)
                {
                    if (x<4)
                        x++;
                }
            else if (key==77)
                {
                    if(y<=4)
                        y++;
                }

            else if(key==75)
            {
                if(y>1)
                    y--;
            }
            else if(key==8)
            {
                x=(-1);
                y=0;
            }
         //   pConsole->gotoLigCol(3,42);
            //std::cout<<x<<";"<<y<<";"<<key;
            pConsole->gotoLigCol(4,42);
            std::cout<<"C'est le tour du joueur "<<a;
            pConsole->gotoLigCol(x+1,4*y-2);
            if (key == 13) // 27 = touche escape
            {
                quit = true;
                *px=x;
                *py=y-1;
                pConsole->gotoLigCol(7,0);
               // std::cout<<*px<<";"<<*py;
            }
        }
    }
}

Case Plateau::getcase(int x, int y)
{
    Case a;
    a=m_plateau[x+1][y];
    return a;
}

void Plateau::poser_pion(Case pion, int x, int y)
{
    char direction;
    int dir; //a
    Personnage* p;
std::cout<<"entrez une direction avec h,b,d,g";
    do
    {

        std::cin>>direction;
    }while(direction!='h' && direction!='b' && direction!='d' && direction!='g');

    p=pion.getpersonnage();
    if(direction=='h')
        dir=1;
    else if(direction=='b')
        dir=(-1);
    else if(direction=='d')
        dir=2;
    else if(direction=='g')
        dir=(-2);
    p->setdirection(dir);
    pion.setPersonnage(p);
    m_plateau[x+1][y]=pion;
}

bool Plateau::case_libre(int x, int y)
{
    bool libre;
    Case temp = m_plateau[x+1][y];
    if((temp.getpersonnage())==NULL)
    {
        libre = true;
    }
    else
        libre = false;
    return libre;
}

void Plateau::supprimer_case(Joueur* j,int x,int y)
{
    Case a;
    int nombre_pions;
    a.setPersonnage(NULL);
    m_plateau[x+1][y]=a;
    nombre_pions=j->getstock_pion();
    j->setstock_pion((nombre_pions+1));
}

void Plateau::deplacer_pion(int x1, int y1, int x2, int y2)
{
    Case temp;
    temp= m_plateau[x1+1][y1]; //On recupere le pion a deplacer dans un tampon
    (m_plateau[x1+1][y1]).setPersonnage(NULL);
    this->poser_pion(temp,(x2),(y2));
}

void Plateau::deplacer_force_pion(int x1, int y1, int x2, int y2)
{
    Case temp;
    temp = m_plateau[x1+1][y1]; //On recupere le pion a deplacer dans un tampon
    (m_plateau[x1+1][y1]).setPersonnage(NULL);
    (m_plateau[x2+1][y2]) = temp;
}
void Plateau::placer_force_pion(Joueur* j,int x, int y, char direction)
{
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    int type_pion=j->gettype_pion();
    int stock_pion,dir;
    Case a;
    stock_pion=j->getstock_pion();
    std::cout<<j->gettype_pion();
    system("PAUSE");
    if(direction=='h')
        dir=1;
    else if(direction=='b')
        dir=(-1);
    else if(direction=='d')
        dir=2;
    else if(direction=='g')
        dir=(-2);
    if(stock_pion>0)
    {
        if(type_pion==1)
        {
            a.setPersonnage(new Elephant(dir));
            this->poser_force_pion(a,x,y);
        }
        if(type_pion==2)
        {
            a.setPersonnage(new Rhinoceros(dir));
            this->poser_force_pion(a,x,y);
        }
        j->setstock_pion((stock_pion)-1);
    }
    else
    {

        pConsole->gotoLigCol(15,0);
        std::cout<<"Vous n'avez plus de pions en stock"<<std::endl;
        system("PAUSE");
    }
}
int Plateau::poussee_force_pion(int x1, int y1, int x2, int y2)
{
    int force=0;
    int cases_a_pousser=0;
    int c=0;
    Case temp=this->getcase(x1,y1);
    Case suiv=this->getcase(x2,y2);
    force=temp.getresistance();
   // std::cout<<suiv.getdirection();
    if(x1==x2)
    {
            if(temp.getdirection()==suiv.getdirection())
            {
                force+=suiv.getresistance();
            }
            else if (temp.getdirection()==-(suiv.getdirection()))
            {
                force-=suiv.getresistance();
            }
            else if(temp.getdirection()==(2)*suiv.getdirection())
            {
                force+=0;
            }
            else if(temp.getdirection()==-(2)*suiv.getdirection())
            {
                force+=0;
            }
     //   std::cout<<std::endl<<force;
        //system("PAUSE");
        if(force>0)
        {

            if(y2==(y1+1)) //si on veut pousser vers la droite
            {
                c=0;
                cases_a_pousser=0;
                while(case_libre(x2,y2+c)==false && y2+c<6)
                {
                    c++;
                    cases_a_pousser++;
                }
                for(int i=cases_a_pousser;i>0;i--) //on parcourt la colonne en partant de la case de droite
            {
                    this->deplacer_force_pion(x2,y2+i-1,x2,y2+i); // on deplace ce personnage vers la droite
            }
                this->deplacer_force_pion(x1,y1,x2,y2); //on se deplace sur cette case
            }

            else if(y2==(y1-1)) //si on veut pousser vers la gauche
            {
                c=0;
                cases_a_pousser=0;
                while(case_libre(x2,y2-c)==false && y2-c>0)
                {
                    c++;
                    cases_a_pousser++;
                }
                for(int i=cases_a_pousser;i>0;i--) //on parcourt la colonne en partant de la case de gauche
            {
                    this->deplacer_force_pion(x2,y2-i+1,x2,y2-i); // on deplace ce personnage vers la gauche
            }
                this->deplacer_force_pion(x1,y1,x2,y2); //on se deplace sur cette case
            }
        }

    }
    else if (y1==y2)
    {
            if(temp.getdirection()==suiv.getdirection())
            {
                force+=suiv.getresistance();
            }
            else if (temp.getdirection()==-(suiv.getdirection()))
            {
                force-=suiv.getresistance();
            }
            else if(temp.getdirection()==(2)*suiv.getdirection())
            {
                force+=0;
            }
            else if(temp.getdirection()==-(2)*suiv.getdirection())
            {
                force+=0;
            }
           // std::cout<<std::endl<<force;
        if(force>0)
        {

            if(x2==(x1+1)) //si on veut pousser vers le bas
            {
                c=0;
                cases_a_pousser=0;
                while(case_libre(x2+c,y2)==false && x2+c<5)
                {
                    c++;
                    cases_a_pousser++;
                }
                for(int i=cases_a_pousser;i>0;i--) //on parcourt la colonne en partant de la case du bas
            {
                    this->deplacer_force_pion(x2+i-1,y2,x2+i,y2); // on deplace ce personnage vers le bas
            }
                this->deplacer_force_pion(x1,y1,x2,y2); //on se deplace sur cette case
            }

            else if(x2==(x1-1)) //si on veut pousser vers le haut
            {
                c=0;
                cases_a_pousser=0;
                while(case_libre(x2-c,y2)==false && x2-c>0)
                {
                    c++;
                    cases_a_pousser++;
                }
                for(int i=cases_a_pousser;i>0;i--) //on parcourt la colonne en partant de la case du haut
            {
                    this->deplacer_force_pion(x2-i+1,y2,x2-i,y2); // on deplace ce personnage vers le haut
            }
                this->deplacer_force_pion(x1,y1,x2,y2); //on se deplace sur cette case
            }
        }

    }
}
void Plateau::poser_force_pion(Case pion, int x, int y)
{
    m_plateau[x+1][y]=pion;
}

void Plateau::poussee_pion(int x1, int y1, int x2, int y2)
{
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    int force=0;
    int cases_a_pousser=0;
    int c=0;
    Case temp=this->getcase(x1,y1);
    Case suiv=this->getcase(x2,y2);
    force=temp.getresistance();
   // std::cout<<suiv.getdirection();
    if(x1==x2)
    {
            if(temp.getdirection()==suiv.getdirection())
            {
                force+=suiv.getresistance();
            }
            else if (temp.getdirection()==-(suiv.getdirection()))
            {
                force-=suiv.getresistance();
            }
            else if(temp.getdirection()==(2)*suiv.getdirection())
            {
                force+=0;
            }
            else if(temp.getdirection()==-(2)*suiv.getdirection())
            {
                force+=0;
            }
            //pConsole->gotoLigCol(9,42);
           // std::cout<<x2<<";"<<y2;
        //std::cout<<std::endl<<force;
        //system("PAUSE");
        if(force>0)
        {
            if(y2==(y1+1)) //si on veut pousser vers la droite
            {
                c=0;
                cases_a_pousser=0;
                while(case_libre(x2,y2+c)==false && y2+c<5)
                {
                    c++;
                    cases_a_pousser++;
                }

                if((y2+cases_a_pousser-1==4) && (case_libre(x2,4)==false))
                {
                    if((this->getcase(x2,4)).gettype()==-1) //Si on sort une montagne
                    {
                        for(int i=3;i>=0;i--)
                        {
                            if(this->getcase(x2,i).getdirection()==2) // haut=1, bas=-1, droite=2, gauche=-2
                        {
                            if(this->getcase(x2,i).gettype()==1) // si l'animal le plus proche qui a pousse la montagne est un elephant
                            {
                                system("cls");
                                std::cout<<" Victoire du Joueur 1 !"<<std::endl;
                                system("PAUSE");
                                menu();
                            }
                            else if(this->getcase(x2,i).gettype()==2) // si l'animal le plus proche qui a pousse la montagne est un rhino
                            {
                                system("cls");
                                std::cout<<" Victoire du Joueur 2 !"<<std::endl;
                                system("PAUSE");
                                menu();
                            }

                        }

                        }

                    }
                }

                for(int i=cases_a_pousser;i>0;i--) //on parcourt la colonne en partant de la case de droite
            {
                    this->deplacer_force_pion(x2,y2+i-1,x2,y2+i); // on deplace ce personnage vers la droite
            }
                this->deplacer_pion(x1,y1,x2,y2); //on se deplace sur cette case
            }

            else if(y2==(y1-1)) //si on veut pousser vers la gauche
            {
                c=0;
                cases_a_pousser=0;
                while(case_libre(x2,y2-c)==false && y2-c>0)
                {
                    c++;
                    cases_a_pousser++;
                }
                 if((y2-c+1==0) && (case_libre(x2,0)==false))
                {
                    if((this->getcase(x2,0)).gettype()==-1) //Si on sort une montagne
                    {
                        for(int i=1;i<5;i++)
                        {
                            if(this->getcase(x2,i).getdirection()==-2) // haut=1, bas=-1, droite=2, gauche=-2
                        {
                            if(this->getcase(x2,i).gettype()==1) // si l'animal le plus proche qui a pousse la montagne est un elephant
                            {
                                system("cls");
                                std::cout<<" Victoire du Joueur 1 !"<<std::endl;
                                system("PAUSE");
                                menu();
                            }
                            else if(this->getcase(x2,i).gettype()==2) // si l'animal le plus proche qui a pousse la montagne est un rhino
                            {
                                system("cls");
                                std::cout<<" Victoire du Joueur 2 !"<<std::endl;
                                system("PAUSE");
                                menu();
                            }

                        }

                        }

                    }
                }
                for(int i=cases_a_pousser;i>0;i--) //on parcourt la colonne en partant de la case de gauche
            {
                    this->deplacer_force_pion(x2,y2-i+1,x2,y2-i); // on deplace ce personnage vers la gauche
            }
                this->deplacer_pion(x1,y1,x2,y2); //on se deplace sur cette case
            }
        }

    }
    else if (y1==y2)
    {
            if(temp.getdirection()==suiv.getdirection())
            {
                force+=suiv.getresistance();
            }
            else if (temp.getdirection()==-(suiv.getdirection()))
            {
                force-=suiv.getresistance();
            }
            else if(temp.getdirection()==(2)*suiv.getdirection())
            {
                force+=0;
            }
            else if(temp.getdirection()==-(2)*suiv.getdirection())
            {
                force+=0;
            }
           // std::cout<<std::endl<<force;
        if(force>0)
        {

            if(x2==(x1+1)) //si on veut pousser vers le bas
            {
                c=0;
                cases_a_pousser=0;
                while(case_libre(x2+c,y2)==false && x2+c<5)
                {
                    c++;
                    cases_a_pousser++;
                }
                if((x2+c-1==4) && (case_libre(4,y2)==false))
                {
                    if((this->getcase(4,y2)).gettype()==-1) //Si on sort une montagne
                    {
                        for(int i=4;i>0;i--)
                        {
                            if(this->getcase(i,y2).getdirection()==-1) // haut=1, bas=-1, droite=2, gauche=-2
                        {
                            if(this->getcase(i,y2).gettype()==1) // si l'animal le plus proche qui a pousse la montagne est un elephant
                            {
                                system("cls");
                                std::cout<<" Victoire du Joueur 1 !";
                                system("PAUSE");
                                menu();
                            }
                            else if(this->getcase(i,y2).gettype()==2) // si l'animal le plus proche qui a pousse la montagne est un rhino
                            {
                                system("cls");
                                std::cout<<" Victoire du Joueur 2 !";
                                system("PAUSE");
                                menu();
                            }

                        }

                        }

                    }
                }

                for(int i=cases_a_pousser;i>0;i--) //on parcourt la colonne en partant de la case du bas
            {
                    this->deplacer_force_pion(x2+i-1,y2,x2+i,y2); // on deplace ce personnage vers le bas
            }
                this->deplacer_pion(x1,y1,x2,y2); //on se deplace sur cette case
            }

            else if(x2==(x1-1)) //si on veut pousser vers le haut
            {
                c=0;
                cases_a_pousser=0;
                while(case_libre(x2-c,y2)==false && x2-c>0)
                {
                    c++;
                    cases_a_pousser++;
                }
                 if((x2-c+1==0) && (case_libre(0,y2)==false))
                {
                    if((this->getcase(0,x2)).gettype()==-1) //Si on sort une montagne
                    {
                        for(int i=1;i<5;i++)
                        {
                            if(this->getcase(i,y2).getdirection()==1) // haut=1, bas=-1, droite=2, gauche=-2
                        {
                            if(this->getcase(i,y2).gettype()==1) // si l'animal le plus proche qui a pousse la montagne est un elephant
                            {
                                system("cls");
                                std::cout<<" Victoire du Joueur 1 !";
                                system("PAUSE");
                                menu();
                            }
                            else if(this->getcase(i,y2).gettype()==2) // si l'animal le plus proche qui a pousse la montagne est un rhino
                            {
                                system("cls");
                                std::cout<<" Victoire du Joueur 2 !";
                                system("PAUSE");
                                menu();
                            }

                        }

                        }

                    }
                }
                for(int i=cases_a_pousser;i>0;i--) //on parcourt la colonne en partant de la case du haut
            {
                    this->deplacer_force_pion(x2-i+1,y2,x2-i,y2); // on deplace ce personnage vers le haut
            }
                this->deplacer_pion(x1,y1,x2,y2); //on se deplace sur cette case
            }
        }

    }
}

void Plateau::placer_pion(Joueur* j,int x, int y)
{
    Console* pConsole=NULL;
    pConsole=Console::getInstance();

    int type_pion=j->gettype_pion();
    int stock_pion;
    Case a;
    stock_pion=j->getstock_pion();
    if(stock_pion>0)
    {
        if(type_pion==1)
        {
            a.setPersonnage(new Elephant);
            this->poser_pion(a,x,y);
        }
        if(type_pion==2)
        {
            a.setPersonnage(new Rhinoceros);
            this->poser_pion(a,x,y);
        }
        j->setstock_pion((stock_pion)-1);
    }
    else
    {

        pConsole->gotoLigCol(15,0);
        std::cout<<"Vous n'avez plus de pions en stock"<<std::endl;
        system("PAUSE");
    }
}

void Plateau::pions_restants(Joueur* j1, Joueur* j2)
{
    Console* pConsole=NULL;
    pConsole=Console::getInstance();
    pConsole->gotoLigCol(15,0);

    int stock_pion_j1, stock_pion_j2;
    stock_pion_j1=j1->getstock_pion();
    stock_pion_j2=j2->getstock_pion();

    std::cout<<"Joueur 1 : "<<stock_pion_j1<<" pions restants"<<std::endl;
    std::cout<<"Joueur 2 : "<<stock_pion_j2<<" pions restants"<<std::endl;
    std::cout<<std::endl;
    system("PAUSE");
}
